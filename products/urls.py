from django.urls import path
from .views import *

urlpatterns = [
    path('', HomeProducts.as_view(), name='home'),
    path('category/<int:category_id>/', ProductsByCategory.as_view(), name='category'),
    path('detail/<int:pk>/', ViewProducts.as_view(), name='detail'),
    path('add-products/', CreateProduct.as_view(), name='add_products'),
    path('update/<int:pk>', UpdateProduct.as_view(), name='update'),
    path('delete/<int:pk>', DeleteProduct.as_view(), name='delete'),

]
