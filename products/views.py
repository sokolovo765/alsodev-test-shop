from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Products, Category
from .forms import ProductsForm
from django.urls import reverse_lazy


class HomeProducts(ListView):
    model = Products
    template_name = 'products/home_products_list.html'
    context_object_name = 'products'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Главная страница'
        return context

    def get_queryset(self):
        return Products.objects.filter(is_published=True)


class ProductsByCategory(ListView):
    model = Products
    template_name = 'products/home_products_list.html'
    context_object_name = 'products'
    allow_empty = False

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = Category.objects.get(pk=self.kwargs['category_id'])
        return context

    def get_queryset(self):
        return Products.objects.filter(category_id=self.kwargs['category_id'], is_published=True)


class ViewProducts(DetailView):
    model = Products
    context_object_name = 'products_item'
    template_name = 'products/products_detail.html'


class CreateProduct(CreateView):
    model = Products
    form_class = ProductsForm
    template_name = 'products/add_products.html'
    success_url = reverse_lazy('home')


class UpdateProduct(UpdateView):
    model = Products
    form_class = ProductsForm
    template_name = "products/update_product.html"
    success_url = reverse_lazy('home')


class DeleteProduct(DeleteView):
    model = Products
    success_url = reverse_lazy('home')
    template_name = 'products/products_confirm_delete.html'
